#include "Plugin.h"
#include "Comm.h"
#include <Windows.h>

bool Plugin::Init(const std::string& filename)
{
	PluginHandle handle = nullptr;
	handle = LoadLibrary(filename.c_str());
	if (handle == nullptr)
		return FAILURE;
	this->handle = handle;
	this->EntryVector.swap(*(PluginInfoVector*)GetProcAddress((HMODULE)handle, str(plugins)));
	return SUCCESS;
}

bool Plugin::Execute(void* &func, int type)
{
	for (auto& entry : this->EntryVector)
	{
		if(type == entry.type)
		{
			func = entry.function;
			return SUCCESS;
		}
	}
	return 0;
}

bool PluginMannager::tuiLoadPlugin(const std::string& filename)
{
	Plugin plug;
	if (FAILURE == plug.Init(filename))
	{
		return FAILURE;
	}
	this->pluginList.push_back(plug);
	return SUCCESS;
}

bool PluginMannager::getFunction(void*& func, int type)
{
	for (auto& plug : pluginList)
	{
		if (SUCCESS == plug.Execute(func, type))
		{
			return SUCCESS;
		}
		else
		{
			return FAILURE;
		}
	}
	return SUCCESS;
}
