#include<iostream>
#include <vector>
#include "Plugin.h"
#include "Comm.h"
using namespace std;
typedef int (*_add_function_)(int a, int b);
typedef int (*_sub_function_)(int a, int b);
int main()
{
	PluginMannager plug;
	if (FAILURE == plug.tuiLoadPlugin("calcu"))
	{
		cout << "Has No DLL" << endl;
	}
	void* func = nullptr;
	if (FAILURE == plug.getFunction(func, ADD_FUNCTION))
	{
		return false;
	}
	int Sum=((_add_function_)func)(1,2);
	cout << "Sum is:" << Sum << endl;

	if (FAILURE == plug.getFunction(func, SUB_FUNCTION))
	{
		return false;
	}
	int Sub= ((_sub_function_)func)(1, 2);
	cout << "Sub is:" << Sub << endl;
	return 0;
}