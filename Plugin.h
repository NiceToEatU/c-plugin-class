#include<iostream>
#include <vector>
using namespace std;
typedef void* PluginHandle;
typedef struct PluginInterfaceInformatin
{
	string dllName = "";
	string version = "";
	string interfaceName = "";
	PluginHandle function = NULL;
	int type = -1;

}PluginInfo;

using PluginInfoVector = std::vector<PluginInfo>;

class Plugin
{
public:
	bool Init(const std::string& filename);
	bool Execute(void* &func, int type);
private:
	PluginHandle handle = nullptr;
	PluginInfoVector EntryVector;
};

class PluginMannager
{
public:
	/*加载插件*/
	bool tuiLoadPlugin(const std::string& filename);
	/*获取函数指针*/
	bool getFunction(void*& func, int type);
private:
	/*插件库*/
	std::vector<Plugin> pluginList;
};
