#include "../Plugin.h"
#include "../Comm.h"
PLUGIN_DEFINE

int add(int x, int y)
{
	return x + y;
}
int sub(int x, int y)
{
	return x - y;
}

PLUGIN_DATA({ "calc", "1.0","add",(void*)add, ADD_FUNCTION }
,{ "calc", "1.0","sub",(void*)sub, SUB_FUNCTION }
);