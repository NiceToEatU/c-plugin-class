#if (_WIN32) || (_WIN64)
#define PLUGIN_EXPORT extern "C" _declspec(dllexport) 
#else
#define PLUGIN_EXPORT exter "C" 
#endif

typedef enum PLUGIN_FUN_TYPE
{
	ADD_FUNCTION = 0,
	SUB_FUNCTION
};

#define __data__ plugins

#define PLUGIN_DATA(...) PluginInfoVector __data__={__VA_ARGS__}

#define PLUGIN_DEFINE					 \
PLUGIN_EXPORT PluginInfoVector	__data__ ;\

#define str(e) #e

#define SUCCESS 0
#define FAILURE 1